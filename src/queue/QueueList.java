/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queue;

/**
 *
 * @author ACER
 */
public class QueueList<T> {

    Node<T> front;
    Node<T> rear;

    public QueueList() {
    }

    public boolean isEmpty() {
        return front == null;
    }

    public void clear() {
//        while(currNode != null){
//            currNode= currNode.next;
//            if(currNode==rear){
//                rear=curr
//            }
//        }
        front.next =null;
        front=null;
    }

    ///Structure: 1 ---> 9 --->6 -----> 8 ---->3 ------>2
    ///             front                                             rear
    //LIFO
    public void enqueue(T node) {
        if (isEmpty()) {
            front = rear = new Node(node, null);
        } else {
            Node<T> newNode = new Node(node, null);
            newNode.next = front;
            front = newNode;
        }
    }

    public void dequeue() {
        if (isEmpty()) {
            System.out.println("Empty");
        } else {
            Node currNode = front;
            while (currNode != rear) {
                currNode = currNode.next;
                if (currNode.next == rear) {
                    currNode.next = null;
                    currNode = rear;
                }
            }
        }
    }

    public void traverse() {
        Node currNode = front;
        if(isEmpty()){
            System.out.println("EmptyArray");
        }
        while (currNode != null) {
            System.out.printf(currNode.info + ", ");
            currNode = currNode.next;
        }
    }

}
