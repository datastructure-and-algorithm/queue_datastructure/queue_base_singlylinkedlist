/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queue;

/**
 *
 * @author ACER
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        QueueList list = new QueueList();
        list.enqueue(5);
        list.enqueue(9);
        list.enqueue(4);
        list.enqueue(7);
        list.enqueue(15);
        list.enqueue(12);
        list.enqueue(2);
        
        System.out.println("\nAfter ENQUEUE 175");
        list.enqueue(175);
        list.traverse();
        
        System.out.println("\nAfter DEQUEUE");
        list.dequeue();
        list.traverse();
        
        System.out.println("\nAfter CLEAR:");
        list.clear();
        list.traverse();
        

    }

}
