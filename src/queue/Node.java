/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queue;

/**
 *
 * @author ACER
 */
public class Node<T> {
    T info;
    Node<T> next;

    public Node(T info, Node<T> next) {
        this.info = info;
        this.next= next;
    }
    
    
}
